# Convert

Tutorial to converter on the console

Install

```
# apt install -y ffmpeg imagemagick pandoc
```

Some flags

```
i - input
o - output
```

Audio

```
$ ffmpeg -i audio.wav audio.mp3
```

Document

```
$ pandoc -o file.pdf file.odt
```

Image

```
$ convert image.png image.jpg
```

Video

```
$ ffmpeg -i video.avi video.mp4
```
